/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React from 'react';

import {
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  useColorScheme,
  View,
  LogBox
} from 'react-native';
import  Navigation from './src/navigation/Navigation'
import FlashMessage from "react-native-flash-message";

import { QueryClient, QueryClientProvider } from "react-query";
const queryClient = new QueryClient();

LogBox.ignoreAllLogs();
const App=()=> {
  const isDarkMode = useColorScheme() === 'dark';


  return (
    <QueryClientProvider client={queryClient}>

      <StatusBar
        barStyle={isDarkMode ? 'light-content' : 'dark-content'}
      />
      <Navigation/>
      <FlashMessage
            position="top"
            hideOnPress={true}
            style={{
              paddingTop: 5,
            }}
            titleStyle={{
              paddingTop: 10,
              alignSelf: "flex-start",
            }}
            textStyle={{
            }}
            floating
          />

    </QueryClientProvider>

  );
};

const styles = StyleSheet.create({
  sectionContainer: {
    marginTop: 32,
    paddingHorizontal: 24,
  },
  sectionTitle: {
    fontSize: 24,
    fontWeight: '600',
  },
  sectionDescription: {
    marginTop: 8,
    fontSize: 18,
    fontWeight: '400',
  },
  highlight: {
    fontWeight: '700',
  },
});

export default App;
