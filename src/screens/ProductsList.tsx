
import React, { FC, useContext, useEffect, useRef, useState } from "react";

import { Text, View, Image, FlatList } from "react-native";
import { ProductComponent } from "../components/ProductComponent/ProdcuctComponent";
import { style } from "./prodcustList/style";
import { usePorducList } from '../api-hocks/hooks';
import { Pixel } from "../utils/constants/styleConstants";


export const ProductList: FC = props => {

    const [productList, setPorductList] = useState([])

    useEffect(() => {
        mutateAsync()
    }, [])
    const { mutateAsync, isLoading, status } = usePorducList((data => {
        setPorductList(data.data.products)
    }))


    return (
        <View style={style.mainView}>
            <View style={[style.listView, { marginTop: Pixel(100), paddingTop: Pixel(15) }]}>

                <FlatList
                    data={productList}
                    renderItem={({ item }) => <ProductComponent{...item} />}
                    style={{

                    }}
                    contentContainerStyle={{

                        paddingBottom: Pixel(40)
                    }}
                    showsHorizontalScrollIndicator={false}
                    showsVerticalScrollIndicator={false}
                    keyExtractor={(item) => item.id}
                />



            </View>
        </View>
    )
}