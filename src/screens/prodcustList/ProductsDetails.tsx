
import React, { FC, useContext, useRef, useState, useEffect } from "react";

import { Text, View, Image, ScrollView, TouchableOpacity } from "react-native";
import { ImageSlider } from "react-native-image-slider-banner";
import { usePorducList, useProductDetails } from "../../api-hocks/hooks";
import { useNavigation, useRoute } from "@react-navigation/native";
import { style } from "./style";
import Swiper from 'react-native-swiper'
import LinearGradient from 'react-native-linear-gradient';
import { IProducDetails } from "../../api-hocks/interface";
import { Colors, Pixel } from "../../utils/constants/styleConstants";

export const ProductDetails: FC = props => {
    const { goBack } = useNavigation();
    interface IState {
        loader: boolean,
        product: IProducDetails | {},
        selectedIndex: number,
        selectedSizeIndex: number

    }

    const [state, setstate] = useState<IState>({
        loader: false,
        product: {},
        selectedIndex: 0,
        selectedSizeIndex: 0

    });

    const colors = [
        Colors.warning,
        Colors.black,
        Colors.primaryBlue,
        Colors.yellow,
    ]

    const sizes = [
        "L",
        "M",
        "S"
    ]
    const route = useRoute()
    const params = route.params

    useEffect(() => {
        mutateAsync()
    }, [])
    const { mutateAsync, isLoading, status } = useProductDetails(params?.id, (data => {
        setstate(old => ({ ...old, product: data.data }));

    }))


    return (
        <View style={style.mainView}>
            <TouchableOpacity style={{ flexDirection: 'row', marginTop: Pixel(60), alignItems:'center'}}
            onPress={()=>{
                goBack()
            }}>
                <Image style={{ width: Pixel(8), marginLeft: Pixel(20) }}
                    source={require('../../assets/Images/back.png')} />
                <Text style={{marginLeft:Pixel(10),color:'white',fontSize:Pixel(18)}}>Project Details</Text>

            </TouchableOpacity>


            <ScrollView style={style.listView}>

                <View style={style.sliderMainView}>

                    <ImageSlider

                        data={[
                            {
                                img: 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRXbmbXyipZMwVE7Vuj3UO0fdMb2sjOrRKdqA&usqp=CAU',
                            },
                            {
                                img: 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTsC6qCKeJlFDs8MWs0iIamQsU9glrCn53aXQ&usqp=CAU',

                            }, {

                                img: 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSyJCbHDU9_nhOPVF9o3X8u2jrA6KifwC4_Tg&usqp=CAU',

                            },

                        ]}
                        autoPlay={true}
                        caroselImageContainerStyle={style.caroselImageContainerStyle}
                        caroselImageStyle={style.caroselImageStyle}
                        previewImageContainerStyle={{ borderRadius: 20, marginHorizontal: 14, }}
                        onItemChanged={(item) => console.log("item", item)}
                        closeIconColor="#fff"
                    />
                </View>
                <View style={{ marginHorizontal: Pixel(24), marginBottom: 50 }}>
                    <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                        <Text style={style.titleTxt}>{state.product?.name}</Text>
                        <Text style={style.priceTxt}>( {state.product?.price} EGP )</Text>

                    </View>
                    <Text style={style.descTxt}>{state.product?.description}</Text>

                    <Text style={style.colorTxt}>color</Text>
                    <View style={style.colorsView}>
                        {
                            colors.map((e, index) => {
                                return (
                                    <TouchableOpacity style={state.selectedIndex == index ? style.selectedColorCard : {}}
                                        onPress={() => {
                                            setstate(old => ({ ...old, selectedIndex: index }))
                                        }}>
                                        <View style={[style.colorCard, { backgroundColor: e }]} />

                                    </TouchableOpacity>
                                )
                            })
                        }


                    </View>
                    <Text style={style.colorTxt}>size</Text>
                    <View style={style.colorsView}>
                        {
                            sizes.map((e, index) => {
                                return (
                                    <TouchableOpacity style={state.selectedSizeIndex == index ? style.selectedSizeCard : style.sizeCard}
                                        onPress={() => {
                                            setstate(old => ({ ...old, selectedSizeIndex: index }))
                                        }}>

                                        <Text>{e}</Text>

                                    </TouchableOpacity>
                                )
                            })
                        }


                    </View>
                    <View style={style.scanView}>
                        <View style={style.scanTitleView}>
                            <Image style={style.imageView}
                                source={require('../../assets/Images/qr.png')} />
                            <View >
                                <Text>
                                    Scan
                                </Text>
                                <Text> get 100 points</Text>
                            </View>

                        </View>
                        <LinearGradient colors={['#00E8DB', '#5C4CDB']} start={{ x: 0, y: 1 }} end={{ x: 1, y: 0 }} style={style.linearGradient}>
                            <Text style={style.textGrideant}>
                                Scan
                            </Text>
                        </LinearGradient>

                    </View>
                    <View style={[style.scanView, { marginTop: Pixel(15) }]}>
                        <View style={style.scanTitleView}>
                            <Image style={style.imageView}
                                source={require('../../assets/Images/amount.png')} />
                            <View >
                                <Text>
                                    Buy & submit
                                </Text>
                                <Text> the receipt for 120 points </Text>
                            </View>

                        </View>
                        <LinearGradient colors={['#00E8DB', '#5C4CDB']} start={{ x: 0, y: 1 }} end={{ x: 1, y: 0 }} style={style.linearGradient}>
                            <Text style={[style.textGrideant, {
                                paddingHorizontal: Pixel(12),
                                paddingVertical: Pixel(10)
                            }]}>
                                add to cart
                            </Text>
                        </LinearGradient>

                    </View>
                </View>


            </ScrollView>

        </View>
    )
}