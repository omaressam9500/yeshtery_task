import React from "react";
import { StyleSheet, Dimensions, I18nManager } from "react-native";
import { Pixel, Colors, } from "../../utils/constants/styleConstants"


const { width, height } = Dimensions.get("window");
export const style = StyleSheet.create({

    mainView: {
        flex: 1,
        backgroundColor: Colors.bkColor,
    },
    listView: {
        backgroundColor: Colors.white,
        borderRadius: Pixel(27),
        marginTop: Pixel(20),
        // paddingHorizontal:Pixel(19),
        flex: 1
    },
    sliderMainView: {
        borderRadius: Pixel(20),
        // marginHorizontal:Pixel(14),
        height: Pixel(300),
        marginTop:Pixel(15)
      


    },
    caroselImageContainerStyle: {
        paddingBottom: 70,
        borderRadius: 20,
      
        //  ,paddingHorizontal:14,
        width: width,
        paddingHorizontal: Pixel(20)
    },
    caroselImageStyle: {
        borderRadius: 20,
        borderColor:Colors.borderSecondColor,
        borderWidth:1,
        width: width * .85,
        overflow: 'hidden',
        alignSelf: 'center',
        resizeMode: 'contain',
        marginHorizontal: 12,

    },
    titleTxt: {
        fontSize: Pixel(16)
    },
    priceTxt: {
        color: Colors.primaryBlue,
        fontSize: Pixel(16)

    },
    descTxt: {
        color: Colors.secondTextColor,
        fontSize: Pixel(14),
        marginTop: Pixel(24)

    },
    colorTxt: {
        marginTop: Pixel(18),
        fontSize: Pixel(16),
        color: Colors.primaryBlue,

    },
    colorsView: {
        flexDirection: 'row',
        marginTop: Pixel(8)
    },
    colorCard: {
        height: Pixel(22),
        width: Pixel(22),
        marginRight: Pixel(9),
        borderRadius: Pixel(11),
        margin: 3


    },
    selectedColorCard: {
        height: Pixel(28),
        width: Pixel(28),
        marginRight: Pixel(9),
        borderRadius: Pixel(14),
        borderColor: 'black',
        borderWidth: 1,
    },
    sizeCard: {
        borderColor: "#C7C7C5",
        borderWidth: 1,
        paddingHorizontal: Pixel(30),
        paddingVertical: Pixel(8),
        borderRadius: Pixel(100),
        marginRight: Pixel(10)
    },
    selectedSizeCard: {
        borderColor: "#5C4CDB",
        borderWidth: 1,
        paddingHorizontal: Pixel(30),
        paddingVertical: Pixel(8),
        borderRadius: Pixel(100),
        marginRight: Pixel(10)
    },
    scanView: {
        flexDirection: 'row',
        justifyContent:'space-between',
        marginTop:Pixel(24)

    },
    imageView:{
        // margin:Pixel(10),
        alignSelf:'center',
        marginRight:Pixel(15),
        resizeMode:'contain'
    },
    scanTitleView: {
        flexDirection: 'row',
        justifyContent:'flex-start',
        alignItems:'center'
    },
    linearGradient:{
        marginVertical:Pixel(10),
        borderRadius:Pixel(8),
    },
    textGrideant:{
        fontSize:Pixel(13),
        color:'white',
        paddingHorizontal:Pixel(30),
        paddingVertical:Pixel(10)
    }





})