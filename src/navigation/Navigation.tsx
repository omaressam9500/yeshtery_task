import React from "react";
import { NavigationContainer, StackActions } from "@react-navigation/native";
import { createNativeStackNavigator } from "@react-navigation/native-stack";
import { Screens } from "../utils/constants/Screens";
import { ProductList } from "../screens/ProductsList";
import { ProductDetails } from "../screens/prodcustList/ProductsDetails";

import { SafeAreaView } from "react-native-safe-area-context";
const Stack = createNativeStackNavigator();


const MainStack = () => {
    return (

        <Stack.Navigator
            initialRouteName={Screens.PRODUCTS_LIST}
            screenOptions={{ headerShown: false }}
        >
            <Stack.Screen name={Screens.PRODUCTS_LIST} component={ProductList} />
            <Stack.Screen name={Screens.PRODUCTS_DETAILS} component={ProductDetails} />

        </Stack.Navigator>

    )
}
const initNavigation = () => {
    return (
        <NavigationContainer>
            <MainStack/>
        </NavigationContainer>
    )
}

export default initNavigation;