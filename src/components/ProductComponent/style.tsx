import React from "react";
import { StyleSheet, Dimensions, I18nManager } from "react-native";
import { Pixel, Colors, } from "../../utils/constants/styleConstants"


const { width, height } = Dimensions.get("window");
export const style = StyleSheet.create({

    mainView: {
        backgroundColor:Colors.white,
        marginVertical:Pixel(7),
        borderRadius:Pixel(8),
        // height:Pixel(129),
        paddingVertical:Pixel(10),
        flexDirection:"row",
        elevation: 2,
        shadowColor: "#000",
        shadowOffset: { width: 0, height: 2 },
        shadowOpacity: 0.08,
        shadowRadius: 10,
        width:width*.9,
        alignSelf:'center',
    },
    dataView:{
        justifyContent:'space-between',
        flex:1,
        // marginVertical:Pixel(20)
    },
    title:{
        fontSize:Pixel(16),

    },
    imageView:{
        // margin:Pixel(10),
        width:Pixel(110),
        alignSelf:'center',
  
        resizeMode:'contain'
    },
    itemImageView:{
        // margin:Pixel(10),
        width:Pixel(30),
        height:(30),
        alignSelf:'center',
      
        resizeMode:'contain'
    },
    dataItemMainView:{
        justifyContent:'center',
        flexDirection:'row'
    },
    dataItemView:{
        backgroundColor:Colors.item_bk,
        paddingLeft:Pixel(7),
        paddingRight:Pixel(10),

        paddingVertical:Pixel(8),
        marginRight:Pixel(15),
        borderRadius:Pixel(6),
        flexDirection:'row',
        alignItems:'center',
        justifyContent:'center',
        color:Colors.item_bk
    }
 
})