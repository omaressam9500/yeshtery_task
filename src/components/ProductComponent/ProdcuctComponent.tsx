
import React, { FC } from 'react'

import { View, Text, Image, TouchableOpacity } from 'react-native'
import { IProduct } from '../../api-hocks/interface';
import { style } from './style'
import { useNavigation } from "@react-navigation/native";
import { Screens } from '../../utils/constants/Screens';




export const ProductComponent: FC<IProduct> = ({
    p_name,
    name,
    price,
    brand_id,
    id
}) => {
    const { navigate } = useNavigation();

    return (
        <TouchableOpacity style={style.mainView}
            onPress={() => {
                navigate(Screens.PRODUCTS_DETAILS, {
                    id: id
                })
            }}>
            <Image style={style.imageView}
                source={require('../../assets/Images/prod_img.png')} />
            <View style={style.dataView}>
                <Text style={style.title}>{name}</Text>
                <View style={style.dataItemMainView}>
                    <View style={style.dataItemView}>
                        <Image style={style.itemImageView}
                            source={require('../../assets/Images/qr.png')} />
                        <Text>{brand_id}</Text>
                    </View>
                    <View style={style.dataItemView}>
                        <Image style={style.itemImageView}
                            source={require('../../assets/Images/amount.png')} />
                        <Text>{price}</Text>
                    </View>
                </View>
            </View>
        </TouchableOpacity>
    )

}