
import { Dimensions, I18nManager, NativeModules, PixelRatio } from 'react-native';

const { width, height } = Dimensions.get('screen');


export enum Colors {
  appBackgroundColor = '#ffffff',
  mainColor = '#00BCB4',
  secondColor = '#254177',
  dangerColor = '#E04F5F',
  mainTextColor = '#686868',
  secondTextColor = '#999999',
  white = '#FFFFFF',
  green = '#21AC17',
  black = '#000000',
  gray = '#0D0E10',
  warning = '#E04F5F',
  sacandAppBackgroundColor = '#F9F9F9',
  CommonBorderColor = '#EDEDED',
  inputBackground = '#FBFBFB',
  hintBackground = '#F4F7FD',
  primaryBlue='#1F54CF',
  diabledColor='#BEBEBE',
  borderSecondColor='#DDE9FF',
  lightRed='#FCEEEF',
  stepColor='#CCF2F0',
  light_blue='#F9FBFF',
  purpel='#A97BF4',
  light_purpel='#F9F5FF',
  yellow='#FEA71C',
  mid_blue="#729EF3",
  item_bk="#FAFAFA",
  bkColor="#4700b3"

  // placeholderColor = ColorWithOpacity("#0D0E10",0.5),
}


export enum Images {}
// categoryImg = require("../assets/images/category-1.png"),
// about = require('../assets/images/about.png'),

export const NavigationAnimations = {
  fade: 'fade',
  fade_from_bottom: 'fade_from_bottom',
  flip: 'flip',
  slide_from_bottom: 'slide_from_bottom',
  slide_from_right: 'slide_from_right',
  slide_from_left: 'slide_from_left',
};

export enum ScreenOptions { 
  StatusBarHeight = NativeModules.StatusBarManager.HEIGHT,
  HalfScreen = width / 2 - 15,
  CURRENT_RESOLUTION = Math.sqrt(height * height + width * width),
  DesignResolution = {
    width: 360,
    height: 640,
  } as any,
}

/**
 * create PerfectPixel fnction from psd or xd workflow size
 * @param designSize uor psd or xd workflow size
 * @returns function to use in PixelPerfect
 */

export const createPerfectPixel = (designSize = { width ,height }) => {
  if (
    !designSize ||
    !designSize.width ||
    !designSize.height ||
    typeof designSize.width !== 'number' ||
    typeof designSize.height !== 'number'
  ) {
    throw new Error(
      'Invalid design size object! must have width and height fields of type Number.',
    );
  }
  const DESIGN_RESOLUTION = Math.sqrt(
    designSize.height * designSize.height + designSize.width * designSize.width,
  );
  const RESOLUTIONS_PROPORTION =
    ScreenOptions.CURRENT_RESOLUTION / DESIGN_RESOLUTION;

  return (size: number) => Math.floor(RESOLUTIONS_PROPORTION * size);
};
/**
 * Get perfect pixel for current resolution
 * @param pixel design size pixel
 * @returns Perfect pixel for current resolution 😄
 */

export const Pixel = (pixel: number) => {
  const Perfect = createPerfectPixel(ScreenOptions.DesignResolution as any);
  return Perfect(pixel);
};


/**
 * create color with opacity
 * @param hex color
 * @param opacity decimal value
 * @returns new color with opacity 👏
 */
export const ColorWithOpacity = (hex: Colors | string, opacity: number) => {
  const result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex);
  let color;
  if (result) {
    color = {
      r: parseInt(result[1], 16),
      g: parseInt(result[2], 16),
      b: parseInt(result[3], 16),
    };
  } else {
    return hex;
  }
  return `rgba(${color.r},${color.g},${color.b},${opacity})`;
};
