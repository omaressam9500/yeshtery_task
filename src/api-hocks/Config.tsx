import axios from "axios";


export const headers = {
  Accept: "application/json",
  "Content-Type": "application/json",
};

export const axiosAPI = axios.create({
    baseURL:"https://api-dev.yeshtery.com/v1/yeshtery/",
    headers: headers,
  });