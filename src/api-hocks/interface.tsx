// https://api-dev.yeshtery.com/v1/yeshtery/products
// https://api-dev.yeshtery.com/v1/yeshtery/product?product_id={id}

export interface ProductsList {
    total: number
    products: IProduct[]
  }
  
  export interface IProduct {
    id: number
    name: string
    image_url: string
    price: number
    prices: Prices
    brand_id: number
    category_id: any
    barcode?: string
    description: string
    discount: number
    currency: number
    stock_id: number
    multiple_variants: boolean
    hidden: boolean
    default_variant_features: any
    tags: Tag[]
    creation_date: string
    update_date: string
    quantity: number
    has_360_view: any
    product_type: number
    product_code?: string
    sku: any
    organization_id: number
    images: Image[]
    rating?: number
    priority: number
    p_name: string
    shop_360s: number[]
  }
  
  export interface Prices {
    min_price: number
    max_price: number
  }
  
  export interface Tag {
    id: number
    name: string
    alias: string
    metadata?: string
    priority: number
    allowReward: boolean
    buyWithCoins: any
    onlyBuyWithCoins: any
    minimumTierId: any
    p_name: string
    category_id: number
    org_id: number
  }
  
  export interface Image {
    id: number
    productId: number
    priority: number
    url: string
    variantId?: number
  }
  export interface Root {
  id: number
  name: string
  image_url: string
  price: number
  prices: any
  brand_id: number
  category_id: any
  barcode: string
  description: string
  discount: number
  currency: number
  stock_id: number
  multiple_variants: boolean
  hidden: boolean
  default_variant_features: DefaultVariantFeatures
  tags: Tag[]
  creation_date: string
  update_date: string
  quantity: any
  has_360_view: any
  product_type: number
  product_code: any
  sku: any
  organization_id: number
  images: Image[]
  rating: any
  priority: number
  variants: Variant[]
  variant_features: VariantFeature[]
  bundle_items: any[]
  p_name: string
  shop_360s: number[]
}

export interface DefaultVariantFeatures {
  size: string
  color: string
}

export interface Tag {
  id: number
  name: string
  alias: string
  metadata?: string
  priority: number
  allowReward: boolean
  buyWithCoins: any
  onlyBuyWithCoins: any
  minimumTierId: any
  p_name: string
  category_id: number
  org_id: number
}

export interface Image {
  id: number
  priority: number
  url: string
}

export interface Variant {
  id: number
  barcode: string
  name: string
  description: string
  sku: any
  product_code: any
  stocks: Stock[]
  images: Image2[]
  extra_attributes: ExtraAttribute[]
  size: string
  color: string
}

export interface Stock {
  id: number
  shop_id: number
  quantity: number
  price: number
  discount: number
  unit: any
  currency_value: string
}

export interface Image2 {
  id: number
  priority: number
  url: string
}

export interface ExtraAttribute {
  id: number
  name: string
  type: string
  icon_url: any
  invisible: boolean
  value: string
}

export interface VariantFeature {
  name: string
  label: string
  type: string
  extra_data: ExtraData
}

export interface ExtraData {
  extra_attribute_id?: number
}

//////// ProducDetails //////////

export interface IProducDetails {
  id: number
  name: string
  image_url: string
  price: number
  prices: any
  brand_id: number
  category_id: any
  barcode: string
  description: string
  discount: number
  currency: number
  stock_id: number
  multiple_variants: boolean
  hidden: boolean
  default_variant_features: DefaultVariantFeatures
  tags: Tag[]
  creation_date: string
  update_date: string
  quantity: any
  has_360_view: any
  product_type: number
  product_code: any
  sku: any
  organization_id: number
  images: Image[]
  rating: any
  priority: number
  variants: Variant[]
  variant_features: VariantFeature[]
  bundle_items: any[]
  p_name: string
  shop_360s: number[]
}

export interface DefaultVariantFeatures {
  size: string
  color: string
}

export interface Tag {
  id: number
  name: string
  alias: string
  metadata?: string
  priority: number
  allowReward: boolean
  buyWithCoins: any
  onlyBuyWithCoins: any
  minimumTierId: any
  p_name: string
  category_id: number
  org_id: number
}

export interface Image {
  id: number
  priority: number
  url: string
}

export interface Variant {
  id: number
  barcode: string
  name: string
  description: string
  sku: any
  product_code: any
  stocks: Stock[]
  images: Image2[]
  extra_attributes: ExtraAttribute[]
  size: string
  color: string
}

export interface Stock {
  id: number
  shop_id: number
  quantity: number
  price: number
  discount: number
  unit: any
  currency_value: string
}

export interface Image2 {
  id: number
  priority: number
  url: string
}

export interface ExtraAttribute {
  id: number
  name: string
  type: string
  icon_url: any
  invisible: boolean
  value: string
}

export interface VariantFeature {
  name: string
  label: string
  type: string
  extra_data: ExtraData
}

export interface ExtraData {
  extra_attribute_id?: number
}
