
import { axiosAPI } from '../api-hocks/Config'
import { useMutation, useQuery } from "react-query";
import { IProducDetails, ProductsList } from './interface'

export const getProductsApi = async (): Promise<ProductsList> => {
  return await axiosAPI.get("products");
};
export const getProductDetailsApi = async (id: number): Promise<IProducDetails> => {
  return await axiosAPI.get(`product?product_id=${id}`);
};

export const usePorducList = (onSuccess: (data: any) => void) => {
  return useMutation(["products-list"], () => getProductsApi(), {
    onError: (error) => {

    },

    onSuccess: (data) => {
      onSuccess(data);

    },
  });
};


export const useProductDetails = (id: number, onSuccess: (data: any) => void) => {
  return useMutation(["products-Details"], () => getProductDetailsApi(id), {
    onError: (error) => {

    },

    onSuccess: (data) => {
      onSuccess(data);

    },
  });
};